﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WebApplication1.Controllers;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsBookWithSameId()
        {
            //Arrange
            var mockRepository = new Mock<IBookRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Book { Id = 10 });
            var controller = new BooksController(mockRepository.Object);

            //Act
            IHttpActionResult actionResult = controller.GetById(10);
            var contentResult = actionResult as OkNegotiatedContentResult<Book>;

            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(10, contentResult.Content.Id);

        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<IBookRepository>();
            var controller = new BooksController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetById(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Book> books = new List<Book>();
            books.Add(new Book { Id = 1, Name = "Bela Ladja", Genre = "Funny", Price = 500, AuthorId = 1 });
            books.Add(new Book { Id = 2, Name = "Bastenska Tehnika", Genre = "Funny", Price = 400, AuthorId = 2 });

            var mockRepository = new Mock<IBookRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(books.AsEnumerable());
            var controller = new BooksController(mockRepository.Object);

            // Act
            IEnumerable<Book> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(books.Count, result.ToList().Count);
            Assert.AreEqual(books.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(books.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange 
            var mockRepository = new Mock<IBookRepository>();
            var controller = new BooksController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<IBookRepository>();
            mockRepository.Setup(x => x.GetById(10)).Returns(new Book { Id = 10 });
            var controller = new BooksController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Delete(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IBookRepository>();
            var controller = new BooksController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(new Book { Name = "Neka tamo" }, 10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<IBookRepository>();
            var controller = new BooksController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Book { Id = 10, Name = "Neki random", Genre = "Funny", Price = 100, AuthorId = 1 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Book>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }
    }
}
