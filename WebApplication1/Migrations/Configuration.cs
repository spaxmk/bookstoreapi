namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<WebApplication1.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WebApplication1.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Authors.AddOrUpdate(
                new Models.Author() { Id = 1, Name = "Ivo Andric" },
                new Models.Author() { Id = 2, Name = "Neki levi"}

                );
            context.SaveChanges();

            context.Books.AddOrUpdate(
                new Models.Book() { Id = 1, Name = "Na drini cuprija", Genre = "Roman", Price = 1200, AuthorId = 1 },
                new Models.Book() { Id = 2, Name = "Krive su zvezde", Genre = "Romance", Price = 600, AuthorId = 2 },
                new Models.Book() { Id = 3, Name = "Harry Poter", Genre = "Fantastic", Price = 2200, AuthorId = 2 },
                new Models.Book() { Id = 4, Name = "Gospodari Prsljenova", Genre = "Fantastic", Price = 1400, AuthorId = 2 },
                new Models.Book() { Id = 5, Name = "Znakovi pored puta", Genre = "Roman", Price = 400, AuthorId = 1 }

                );
            context.SaveChanges();
        }
    }
}
