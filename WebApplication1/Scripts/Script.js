﻿$(window).on(function () {
    $("#book").click();
});

$(document).ready(function () {
    // podaci od interesa
    var host = window.location.host;
    var token = null;
    var headers = {};
    var formAction = "Create"; // dodavanje proizvoda
    var editingId; // id za PUT

    // posto inicijalno nismo prijavljeni, sakrivamo odjavu
    $("#odjava").css("display", "none");
    $("#bReg").click(function () {
        $("#registracijaP").css("display", "block");
        $("#bReg").css("display", "none");
    });

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };


        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            clearForm();
            $("#info").append("Uspešna registracija. Možete se prijaviti na sistem.");

        }).fail(function (data) {
            alert(data);
        });
    });

    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            console.log(data);
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#odjava").css("display", "block");

            $("#book").trigger("click");

        }).fail(function (data) {
            alert(data);
        });
    });

    // odjava korisnika sa sistema
    $("#odjavise").click(function () {
        token = null;
        headers = {};

        $("#info").empty();
        $("#data").empty();
        $("#book").trigger("click");
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "block");
        $("#odjava").css("display", "none");
        $("#formDiv").css("display", "none");
        $("#priEmail").val('');
        $("#priLoz").val('');

    })

    // pripremanje dogadjaja za brisanje
    $("body").on("click", "#btnDelete", deleteBook);

    // priprema dogadjaja za izmenu
    $("body").on("click", "#btnEdit", editBook);

    // ucitavanje proizvoda
    $("#book").click(function () {

        // korisnik mora biti ulogovan
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/books/details",
            "headers": headers

        }).done(function (data, status) {
            clearForm();
            var $container = $("#data");
            $container.empty();

            if (status === "success") {
                console.log(data);
                // ispis Proizvoda
                if (token) {
                    var div = $("<div></div>");
                    var h1 = $("<h1>Knjige</h1>");
                    div.append(h1);

                    // ispis tabele
                    var table = $("<table class='table table-hover table-bordered'></table>");
                    var header = $("<tr><th>Id</th><th>Naziv</th><th>Cena</th><th>Zanr</th><th>Autor</th><th>Delete</th><th>Edit</th></tr>");

                    table.append(header);

                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        var row = "<tr>";
                        // prikaz podataka
                        var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td><td>" + data[i].Genre + "</td><td>" + data[i].AuthorId + "</td>";
                        // prikaz dugmadi za izmenu i brisanje
                        var stringId = data[i].Id.toString();
                        var displayDelete = "<td><button class='btn-danger' id=btnDelete name=" + stringId + ">Delete</button></td>";
                        var displayEdit = "<td><button class='btn-primary' id=btnEdit name=" + stringId + ">Edit</button></td>";
                        row += displayData + displayDelete + displayEdit + "</tr>";
                        table.append(row);
                        newId = data[i].Id;

                        console.log(data);
                    }

                    div.append(table);

                    // prikaz forme
                    $("#formDiv").css("display", "block");

                    // ispis novog sadrzaja
                    $container.append(div);

                } else {
                    div = $("<div></div>");
                    h1 = $("<h1>Knjige</h1>");
                    div.append(h1);

                    // ispis tabele
                    table = $("<table class='table table-hover table-bordered'></table>");
                    header = $("<tr><th>ID</th><th>Naziv</th><th>Cena</th><th>Zanr</th><th>Autor</th></tr>");
                    table.append(header);

                    for (i = 0; i < data.length; i++) {
                        // prikazujemo novi red u tabeli
                        row = "<tr>";
                        // prikaz podataka
                        displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Name + "</td><td>" + data[i].Price + "</td><td>" + data[i].Genre + "</td><td>" + data[i].AuthorId + "</td>";
                        stringId = data[i].Id.toString();

                        row += displayData + "</tr>";
                        table.append(row);
                        newId = data[i].Id;

                        console.log(data);
                    }

                    div.append(table);

                    // ispis novog sadrzaja
                    $container.append(div);
                }

            }
            else {
                div = $("<div></div>");
                h1 = $("<h1>Greška prilikom preuzimanja knjiga!</h1>");
                div.append(h1);
                $container.append(div);
            }

        }).fail(function (data) {
            alert(data.status + ": " + data.statusText);
        });


    });

    // Brisanje Proizvoda
    function deleteBook() {

        // izvlacimo {id}
        var deleteId = this.name;
        // saljemo zahtev 
        $.ajax({
            "type": "DELETE",
            "url": "http://" + host + "/api/books/" + deleteId.toString(),
            "headers": headers
        }).done(function (data, status) {
            refreshTable();
        }).fail(function (data, status) {
            alert("Desila se greska!");
        });

    };

    // Izmena Proizvoda
    function editBook() {
        // izvlacimo id
        var editId = this.name;
        // saljemo zahtev da dobavimo taj proizvod
        $.ajax({
            "type": "GET",
            "url": "http://" + host + "/api/books/" + editId.toString(),
            "headers": headers
        }).done(function (data, status) {
            $("#bookNaziv").val(data.Name);
            $("#bookCena").val(data.Price);
            $("#bookZanr").val(data.Genre);
            $("#bookAutor").val(data.AuthorId);
            editingId = data.Id;
            formAction = "Update";
        }).fail(function (data, status) {
            formAction = "Create";
            alert("Desila se greska!");
        });

    };

    // add i edit proizvoda
    $("#bookForm").submit(function (e) {
        // sprecavanje default akcije forme
        e.preventDefault();

        var bookNaziv = $("#bookNaziv").val();
        var bookCena = $("#bookCena").val();
        var bookZanr = $("#bookZanr").val();
        var bookAutor = $("#bookAutor").val();
        var httpAction;
        var sendData;
        var url;

        // u zavisnosti od akcije pripremamo objekat
        if (formAction === "Create") {
            httpAction = "POST";
            url = "http://" + host + "/api/books/";
            console.log("URL:" + url);
            sendData = {
                "Name": bookNaziv,
                "Price": bookCena,
                "Genre": bookZanr,
                "AuthorId": bookAutor
            };
        }
        else {
            httpAction = "PUT";
            url = "http://" + host + "/api/books/" + editingId.toString();
            sendData = {
                "Id": editingId,
                "Name": bookNaziv,
                "Price": bookCena,
                "Genre": bookZanr,
                "AuthorId": bookAutor
            };
        }

        console.log("Objekat za slanje");
        console.log(sendData);

        $.ajax({
            url: url,
            type: httpAction,
            "headers": headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                refreshTable();
            })
            .fail(function (data, status) {
                alert("Desila se greska!");
            })

    });

    // osvezavanje prikaz tabele
    function refreshTable() {
        // cistimo formu
        $("#bookNaziv").val('');
        $("#bookCena").val('');
        $("#bookZanr").val('');
        $("#bookAutor").val('');

        // osvezavamo
        $("#book").trigger("click");
    };

    // osvezavanje forme
    function clearForm() {
        // clear forme
        $("#bookNaziv").val('');
        $("#bookPrice").val('');
        $("#bookZanr").val('');
        $("#bookAutor").val('');

        // clear registracija
        $("#regEmail").val('').empty();
        $("#regLoz").val('').empty();
        $("#regLoz2").val('').empty();
    };

});