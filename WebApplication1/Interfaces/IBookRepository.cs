﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Interfaces
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAll();
        IEnumerable<BookDTO> BookDTO();
        IEnumerable<Book> FilterPrice(int x, int y);
        Book GetById(int Id);
        void Add(Book book);
        void Update(Book book);
        void Delete(Book book);

    }
}
