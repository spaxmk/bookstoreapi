﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class BooksController : ApiController
    {
        private IBookRepository _repository { get; set; }
        public BooksController(IBookRepository repository)
        {
            _repository = repository;
        }

        [ResponseType(typeof(Book))]
        [Route("api/books/details")]
        public IEnumerable<Book> GetAll()
        {
            return _repository.GetAll();
        }

        [ResponseType(typeof(Book))]
        public IHttpActionResult GetById(int Id)
        {
            var book = _repository.GetById(Id);
            if (book == null)
            {
                return NotFound();
            }
            return Ok(book);
        }

        [ResponseType(typeof(Book))]
        public IHttpActionResult Post(Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(book);
            return Ok(book);
        }

        [ResponseType(typeof(Book))]
        public IHttpActionResult Put(Book book, int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            if (book.Id != Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(book);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
            
        }

        [Route("api/books")]
        // GET api/Books
        public IEnumerable<BookDTO> GetBooks()
        {
            return _repository.BookDTO();
        }

        [ResponseType(typeof(Book))]
        [Route("api/books/filter")]
        public IEnumerable<Book> GetFilterPrice(int x, int y)
        {
            return _repository.FilterPrice(x, y);
        }


        public IHttpActionResult Delete(int Id)
        {
            var book = _repository.GetById(Id);
            if (book == null)
            {
                return NotFound();
            }
            _repository.Delete(book);
            return Ok();
        }
    }
}
