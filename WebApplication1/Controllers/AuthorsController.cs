﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AuthorsController : ApiController
    {
        private IAuthorRepository _repository { get; set; }
        public AuthorsController(IAuthorRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Author> GetAll()
        {
            return _repository.GetAll();
        }

        [ResponseType(typeof(Author))]
        public IHttpActionResult GetById(int Id)
        {
            var author = _repository.GetById(Id);
            if (author == null)
            {
                return NotFound();
            }
            return CreatedAtRoute("DefaultApi", new { Id = author.Id }, author);
        }

        [ResponseType(typeof(Author))]
        public IHttpActionResult Post(Author author)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _repository.Add(author);
            return Ok();
        }

        [ResponseType(typeof(Author))]
        public IHttpActionResult Put(Author author, int Id)
        {
            if (!ModelState.IsValid)
            {

                return BadRequest();
            }
            if (author.Id != Id)
            {
                return BadRequest();
            }
            try
            {
                _repository.Update(author);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            return Ok();
        }

        [ResponseType(typeof(Author))]
        public IHttpActionResult Delete(int Id)
        {
            var author = _repository.GetById(Id);
            if (author == null)
            {
                return BadRequest();
            }
            _repository.Delete(author);
            return Ok();
        }
    }
}
