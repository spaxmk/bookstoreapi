﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Repository
{
    public class BookRepository : IDisposable, IBookRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Book book)
        {
            db.Books.Add(book);
            db.SaveChanges();
        }

        public void Delete(Book book)
        {
            db.Books.Remove(book);
            db.SaveChanges();
        }

        public IEnumerable<Book> GetAll()
        {
            return db.Books;
        }

        public Book GetById(int Id)
        {
            return db.Books.FirstOrDefault(b => b.Id == Id);
        }

        public void Update(Book book)
        {
            db.Entry(book).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
        }

        

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        

        public IEnumerable<BookDTO> BookDTO()
        {
            var books = db.Books.Include(b => b.Author).Select(d => new BookDTO
            {
                Id = d.Id,
                Name = d.Name,
                AuthorName = d.Author.Name
            });
            return books;
        }

        public IEnumerable<Book> FilterPrice(int x, int y)
        {
            IEnumerable<Book> books = from f in db.Books where f.Price > x && f.Price < y select f;
            return books;
        }
    }
}