﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Repository
{
    public class AuthorRepository : IDisposable, IAuthorRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public void Add(Author author)
        {
            db.Authors.Add(author);
            db.SaveChanges();
        }

        public void Delete(Author author)
        {
            db.Authors.Remove(author);
            db.SaveChanges();
        }

        public IEnumerable<Author> GetAll()
        {
            return db.Authors;
        }

        public Author GetById(int Id)
        {
            return db.Authors.FirstOrDefault(a=>a.Id==Id);
        }

        public void Update(Author author)
        {
            db.Entry(author).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}